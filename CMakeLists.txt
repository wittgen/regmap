cmake_minimum_required(VERSION 3.25)
include(FetchContent)
find_package(Python 3.8 COMPONENTS Interpreter Development.Module REQUIRED)
execute_process(
  COMMAND "${Python_EXECUTABLE}" -m nanobind --cmake_dir
  OUTPUT_STRIP_TRAILING_WHITESPACE OUTPUT_VARIABLE NB_DIR)
list(APPEND CMAKE_PREFIX_PATH "${NB_DIR}")
FetchContent_Declare(
  glaze
  GIT_REPOSITORY https://github.com/stephenberry/glaze.git
  GIT_TAG main
  GIT_SHALLOW TRUE
)
FetchContent_MakeAvailable(glaze)
project(daq)
find_package(nanobind CONFIG REQUIRED)
set(CMAKE_CXX_STANDARD 20)

add_executable(daq main.cpp)
target_compile_options(daq PRIVATE -fbracket-depth=512)
target_link_libraries(daq PRIVATE glaze::glaze)
nanobind_add_module(rd53bmod rd53bmod.cpp)



