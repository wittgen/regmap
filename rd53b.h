#pragma once

#include <array>
#include <cstdint>
#include <string_view>
#include <string_view>


template<typename T>
struct  Register_ {
    constexpr Register_(const T _enum_val, uint16_t _addr, unsigned _offset, unsigned _bits, unsigned _value) :
            enum_val(_enum_val), addr(_addr), offset(_offset), bits(_bits), value(_value) {
    }
    [[maybe_unused]] const T enum_val;
    unsigned addr;
    unsigned bits;
    unsigned offset;
    uint16_t value;
};
enum GlobalRegisterEnum {
    PixPortal, 
    PixRegionCol, 
    PixRegionRow, 
    PixBroadcast, 
    PixConfMode, 
    PixAutoRow, 
    PixDefaultConfig, 
    PixDefaultConfigB, 
    GcrDefaultConfig,
    GcrDefaultConfigB,
    DiffPreampL,
    DiffPreampR,
    DiffPreampTL,
    DiffPreampTR,
    DiffPreampT,
    DiffPreampM,
    DiffPreComp,
    DiffComp,
    DiffVff,
    DiffTh1L,
    DiffTh1R,
    DiffTh1M,
    DiffTh2,
    DiffLcc,
    DiffLccEn,
    DiffFbCapEn,
    LinPreampL,
    LinPreampR,
    LinPreampTL,
    LinPreampTR,
    LinPreampT,
    LinPreampM,
    LinFc,
    LinKrumCurr,
    LinRefKrum,
    LinComp,
    LinCompTa,
    LinGdacL,
    LinGdacR,
    LinGdacM,
    LinLdac,
    SldoEnUndershuntA,
    SldoEnUndershuntD,
    SldoTrimA,
    SldoTrimD,
    EnCoreCol3,
    EnCoreCol2,
    EnCoreCol1,
    EnCoreCol0,
    RstCoreCol3,
    RstCoreCol2,
    RstCoreCol1,
    RstCoreCol0,
    TwoLevelTrig,
    Latency,
    SelfTrigEn,
    SelfTrigDigThrEn,
    SelfTrigDigThr,
    SelfTrigDelay,
    SelfTrigMulti, 
    SelfTrigPattern,
    DataReadDelay,
    ReadTrigLatency,
    TruncTimeoutConf,
    InjDigEn,
    InjAnaMode,
    InjFineDelay,
    FineDelayClk,
    FineDelayData,
    InjVcalHigh,
    InjVcalMed,
    CapMeasEnPar,
    CapMeasEn,
    InjVcalRange,
    CdrOverwriteLimit,
    CdrPhaseDetSel,
    CdrClkSel,
    ChSyncLockThr,
    GlobalPulseConf,
    GlobalPulseWidth,
    ServiceBlockEn,
    ServiceBlockPeriod,
    TotEnPtot,
    TotEnPtoa,
    TotEn80,
    TotEn6b4b,
    TotPtotLatency,
    PtotCoreColEn3,
    PtotCoreColEn2,
    PtotCoreColEn1,
    PtotCoreColEn0,
    DataMergeInPol,
    EnChipId,
    DataMergeEnClkGate,
    DataMergeSelClk,
    DataMergeEn,
    DataMergeEnBond,
    DataMergeInMux3,
    DataMergeInMux2,
    DataMergeInMux1,
    DataMergeInMux0,
    DataMergeOutMux3,
    DataMergeOutMux2,
    DataMergeOutMux1,
    DataMergeOutMux0,
    EnCoreColCal3,
    EnCoreColCal2,
    EnCoreColCal1,
    EnCoreColCal0,
    DataEnBcid,
    DataEnL1id,
    DataEnEos,
    NumOfEventsInStream,
    DataEnBinaryRo,
    DataEnRaw,
    DataEnHitRemoval,
    DataMaxHits,
    DataEnIsoHitRemoval,
    DataMaxTot,
    EvenMask,
    OddMask,
    EfuseConfig,
    EfuseWriteData1,
    EfuseWriteData0,
    AuroraEnPrbs,
    AuroraActiveLanes,
    AuroraCCWait,
    AuroraCCSend,
    AuroraCBWait1,
    AuroraCBWait0,
    AuroraCBSend,
    AuroraInitWait,
    GpValReg,
    GpCmosEn,
    GpCmosDs,
    GpLvdsEn,
    GpLvdsBias,
    GpCmosRoute,
    GpLvdsPad3,
    GpLvdsPad2,
    GpLvdsPad1,
    GpLvdsPad0,
    CdrCp,
    CdrCpFd,
    CdrCpBuff,
    CdrVco,
    CdrVcoBuff,
    SerSelOut3,
    SerSelOut2,
    SerSelOut1,
    SerSelOut0,
    SerInvTap,
    SerEnTap,
    SerEnLane,
    CmlBias2,
    CmlBias1,
    CmlBias0,
    MonitorEnable,
    MonitorI,
    MonitorV,
    ErrWngMask,
    MonSensSldoDigEn,
    MonSensSldoDigDem,
    MonSensSldoDigSelBias,
    MonSensSldoAnaEn,
    MonSensSldoAnaDem,
    MonSensSldoAnaSelBias,
    MonSensAcbEn,
    MonSensAcbDem,
    MonSensAcbSelBias,
    VrefRsensBot,
    VrefRsensTop,
    VrefIn,
    MonAdcTrim,
    NtcDac,
    HitOrMask3,
    HitOrMask2,
    HitOrMask1,
    HitOrMask0,
    AutoRead0,
    AutoRead1,
    AutoRead2,
    AutoRead3,
    AutoRead4,
    AutoRead5,
    AutoRead6,
    AutoRead7,
    RingOscBClear,
    RingOscBEnBl,
    RingOscBEnBr,
    RingOscBEnCapA,
    RingOscBEnFf,
    RingOscBEnLvt,
    RingOscAClear,
    RingOscAEn,
    RingOscARoute,
    RingOscBRoute,
    RingOscAOut,
    RingOscBOut,
    BcidCnt,
    TrigCnt,
    ReadTrigCnt,
    LockLossCnt,
    BitFlipWngCnt,
    BitFlipErrCnt,
    CmdErrCnt,
    RdWrFifoErrCnt,
    AiRegionRow,
    HitOrCnt3,
    HitOrCnt2,
    HitOrCnt1,
    HitOrCnt0,
    SkippedTrigCnt,
    EfuseReadData1,
    EfuseReadData0, 
    MonitoringDataAdc,
    COUNT
};

template <typename E, E V> static inline constexpr std::string_view enum_name() {
    std::string_view func(__PRETTY_FUNCTION__, sizeof(__PRETTY_FUNCTION__)-1);
#ifdef __clang__
    const auto start  = func.find("V = ") + 4 ;
    const auto end = func.find(']');
#else
    const auto start  = func.find("E V = ") + 6 ;
    const auto end = func.find("; std");
#endif
    std::string_view name(&func[start] , end - start);
    return name ;
}
template <typename E>
using enum_string_list = std::array<std::string_view, static_cast<std::size_t>(E::COUNT)>;


constexpr std::string_view t1 = enum_name<GlobalRegisterEnum ,GlobalRegisterEnum::PixPortal>();

template <typename E, std::size_t... Ns> constexpr enum_string_list<E> enum_name_list(std::index_sequence<Ns...>) {
    constexpr auto N = sizeof...(Ns);
    std::array<std::string_view, N> result = { enum_name<E, (E)Ns>()...};
    return result;
}

class GlobalRegister {
public:
    static inline constexpr unsigned N_PROPERTIES = 216;
    static inline constexpr unsigned N_REGS = 138;
    using RegArray = std::array<uint16_t, N_REGS>;
    using RegArrayUnpacked = std::array<uint16_t, N_PROPERTIES>;
    using Register = Register_<GlobalRegisterEnum>;
    static inline GlobalRegisterEnum e;
    static inline constexpr std::array<Register, N_PROPERTIES> regMap{
            Register{PixPortal, 0, 0, 16, 0},
            Register{PixRegionCol, 1, 0, 16, 0},
            Register{PixRegionRow, 2, 0, 16, 0},
            Register{PixBroadcast, 3, 2, 1, 0},
            Register{PixConfMode, 3, 1, 1, 1},
            Register{PixAutoRow, 3, 0, 1, 0},
            Register{PixDefaultConfig, 4, 0, 16, 0x9CE2},
            Register{PixDefaultConfigB, 5, 0, 16, 0x631D},
            Register{GcrDefaultConfig, 6, 0, 16, 0xAC75},
            Register{GcrDefaultConfigB, 7, 0, 16, 0x538A},
            Register{DiffPreampL, 8, 0, 10, 800},
            Register{DiffPreampR, 9, 0, 10, 800},
            Register{DiffPreampTL, 10, 0, 10, 800},
            Register{DiffPreampTR, 11, 0, 10, 800},
            Register{DiffPreampT, 12, 0, 10, 800},
            Register{DiffPreampM, 13, 0, 10, 800},
            Register{DiffPreComp, 14, 0, 10, 350},
            Register{DiffComp, 15, 0, 10, 500},
            Register{DiffVff, 16, 0, 10, 160},
            Register{DiffTh1L, 17, 0, 10, 350},
            Register{DiffTh1R, 18, 0, 10, 350},
            Register{DiffTh1M, 19, 0, 10, 350},
            Register{DiffTh2, 20, 0, 10, 50},
            Register{DiffLcc, 21, 0, 10, 100},
            Register{DiffLccEn, 37, 1, 1, 0},
            Register{DiffFbCapEn, 37, 0, 1, 0},
            Register{LinPreampL, 22, 0, 10, 300},
            Register{LinPreampR, 23, 0, 10, 300},
            Register{LinPreampTL, 24, 0, 10, 300},
            Register{LinPreampTR, 25, 0, 10, 300},
            Register{LinPreampT, 26, 0, 10, 300},
            Register{LinPreampM, 27, 0, 10, 300},
            Register{LinFc, 28, 0, 10, 20},
            Register{LinKrumCurr, 29, 0, 10, 50},
            Register{LinRefKrum, 30, 0, 10, 300},
            Register{LinComp, 31, 0, 10, 110},
            Register{LinCompTa, 32, 0, 10, 110},
            Register{LinGdacL, 33, 0, 10, 408},
            Register{LinGdacR, 34, 0, 10, 408},
            Register{LinGdacM, 35, 0, 10, 408},
            Register{LinLdac, 36, 0, 10, 100},
            Register{SldoEnUndershuntA, 38, 9, 1, 0},
            Register{SldoEnUndershuntD, 38, 8, 1, 0},
            Register{SldoTrimA, 38, 4, 4, 8},
            Register{SldoTrimD, 38, 0, 4, 8},
            Register{EnCoreCol3, 39, 0, 6, 63},
            Register{EnCoreCol2, 40, 0, 16, 65535},
            Register{EnCoreCol1, 41, 0, 16, 65535},
            Register{EnCoreCol0, 42, 0, 16, 65535},
            Register{RstCoreCol3, 43, 0, 6, 0},
            Register{RstCoreCol2, 44, 0, 16, 0},
            Register{RstCoreCol1, 45, 0, 16, 0},
            Register{RstCoreCol0, 46, 0, 16, 0},
            Register{TwoLevelTrig, 47, 9, 1, 0},
            Register{Latency, 47, 0, 9, 500},
            Register{SelfTrigEn, 48, 5, 1, 0},
            Register{SelfTrigDigThrEn, 48, 4, 1, 0},
            Register{SelfTrigDigThr, 48, 0, 4, 1},
            Register{SelfTrigDelay, 49, 5, 10, 512},
            Register{SelfTrigMulti, 49, 0, 5, 4},
            Register{SelfTrigPattern, 50, 0, 16, 65534},
            Register{DataReadDelay, 51, 12, 2, 0},
            Register{ReadTrigLatency, 51, 0, 12, 1000},
            Register{TruncTimeoutConf, 52, 0, 12, 0},
            Register{InjDigEn, 53, 7, 1, 0},
            Register{InjAnaMode, 53, 6, 1, 0},
            Register{InjFineDelay, 53, 0, 6, 5},
            Register{FineDelayClk, 54, 6, 6, 0},
            Register{FineDelayData, 54, 0, 6, 0},
            Register{InjVcalHigh, 55, 0, 12, 200},
            Register{InjVcalMed, 56, 0, 12, 200},
            Register{CapMeasEnPar, 57, 2, 1, 0},
            Register{CapMeasEn, 57, 1, 1, 0},
            Register{InjVcalRange, 57, 0, 1, 1},
            Register{CdrOverwriteLimit, 58, 4, 1, 0},
            Register{CdrPhaseDetSel, 58, 3, 1, 0},
            Register{CdrClkSel, 58, 0, 3, 1},
            Register{ChSyncLockThr, 59, 0, 5, 31},
            Register{GlobalPulseConf, 60, 0, 16, 0},
            Register{GlobalPulseWidth, 61, 0, 8, 0},
            Register{ServiceBlockEn, 62, 8, 1, 1},
            Register{ServiceBlockPeriod, 62, 0, 8, 50},
            Register{TotEnPtot, 63, 12, 1, 0},
            Register{TotEnPtoa, 63, 11, 1, 0},
            Register{TotEn80, 63, 10, 1, 0},
            Register{TotEn6b4b, 63, 9, 1, 0},
            Register{TotPtotLatency, 63, 0, 9, 500},
            Register{PtotCoreColEn3, 64, 0, 6, 0},
            Register{PtotCoreColEn2, 65, 0, 16, 0},
            Register{PtotCoreColEn1, 66, 0, 16, 0},
            Register{PtotCoreColEn0, 67, 0, 16, 0},
            Register{DataMergeInPol, 68, 8, 4, 0},
            Register{EnChipId, 68, 7, 1, 0},
            Register{DataMergeEnClkGate, 68, 6, 1, 0},
            Register{DataMergeSelClk, 68, 5, 1, 0},
            Register{DataMergeEn, 68, 1, 4, 0},
            Register{DataMergeEnBond, 68, 0, 1, 0},
            Register{DataMergeInMux3, 69, 14, 2, 3},
            Register{DataMergeInMux2, 69, 12, 2, 2},
            Register{DataMergeInMux1, 69, 10, 2, 1},
            Register{DataMergeInMux0, 69, 8, 2, 0},
            Register{DataMergeOutMux3, 69, 6, 2, 0},
            Register{DataMergeOutMux2, 69, 4, 2, 1},
            Register{DataMergeOutMux1, 69, 2, 2, 2},
            Register{DataMergeOutMux0, 69, 0, 2, 3},
            Register{EnCoreColCal3, 70, 0, 6, 0},
            Register{EnCoreColCal2, 71, 0, 16, 0},
            Register{EnCoreColCal1, 72, 0, 16, 0},
            Register{EnCoreColCal0, 73, 0, 16, 0},
            Register{DataEnBcid, 74, 10, 1, 0},
            Register{DataEnL1id, 74, 9, 1, 0},
            Register{DataEnEos, 74, 8, 1, 1},
            Register{NumOfEventsInStream, 74, 0, 8, 1},
            Register{DataEnBinaryRo, 75, 10, 1, 0},
            Register{DataEnRaw, 75, 9, 1, 0},
            Register{DataEnHitRemoval, 75, 8, 1, 0},
            Register{DataMaxHits, 75, 4, 4, 0},
            Register{DataEnIsoHitRemoval, 75, 3, 1, 0},
            Register{DataMaxTot, 75, 0, 3, 0},
            Register{EvenMask, 76, 0, 16, 0},
            Register{OddMask, 77, 0, 16, 0},
            Register{EfuseConfig, 78, 0, 16, 0},
            Register{EfuseWriteData1, 79, 0, 16, 0},
            Register{EfuseWriteData0, 80, 0, 16, 0},
            Register{AuroraEnPrbs, 81, 12, 1, 0},
            Register{AuroraActiveLanes, 81, 8, 4, 15},
            Register{AuroraCCWait, 81, 2, 6, 25},
            Register{AuroraCCSend, 81, 0, 2, 3},
            Register{AuroraCBWait1, 82, 0, 8, 0},
            Register{AuroraCBWait0, 83, 4, 12, 4095},
            Register{AuroraCBSend, 83, 0, 4, 0},
            Register{AuroraInitWait, 84, 0, 11, 32},
            Register{GpValReg, 85, 9, 4, 5},
            Register{GpCmosEn, 85, 8, 1, 1},
            Register{GpCmosDs, 85, 7, 1, 0},
            Register{GpLvdsEn, 85, 3, 4, 0xF},
            Register{GpLvdsBias, 85, 0, 3, 7},
            Register{GpCmosRoute, 86, 0, 7, 34},
            Register{GpLvdsPad3, 87, 6, 6, 35},
            Register{GpLvdsPad2, 87, 0, 6, 33},
            Register{GpLvdsPad1, 88, 6, 6, 1},
            Register{GpLvdsPad0, 88, 0, 6, 0},
            Register{CdrCp, 89, 0, 10, 40},
            Register{CdrCpFd, 90, 0, 10, 400},
            Register{CdrCpBuff, 91, 0, 10, 200},
            Register{CdrVco, 92, 0, 10, 1023},
            Register{CdrVcoBuff, 93, 0, 10, 500},
            Register{SerSelOut3, 94, 6, 2, 1},
            Register{SerSelOut2, 94, 4, 2, 1},
            Register{SerSelOut1, 94, 2, 2, 1},
            Register{SerSelOut0, 94, 0, 2, 1},
            Register{SerInvTap, 95, 6, 2, 0},
            Register{SerEnTap, 95, 4, 2, 0},
            Register{SerEnLane, 95, 0, 4, 15},
            Register{CmlBias2, 96, 0, 10, 0},
            Register{CmlBias1, 97, 0, 10, 0},
            Register{CmlBias0, 98, 0, 10, 500},
            Register{MonitorEnable, 99, 12, 1, 0},
            Register{MonitorI, 99, 6, 6, 63},
            Register{MonitorV, 99, 0, 6, 63},
            Register{ErrWngMask, 100, 0, 8, 0},
            Register{MonSensSldoDigEn, 101, 11, 1, 0},
            Register{MonSensSldoDigDem, 101, 7, 4, 0},
            Register{MonSensSldoDigSelBias, 101, 6, 1, 0},
            Register{MonSensSldoAnaEn, 101, 5, 1, 0},
            Register{MonSensSldoAnaDem, 101, 1, 4, 0},
            Register{MonSensSldoAnaSelBias, 101, 0, 1, 0},
            Register{MonSensAcbEn, 102, 5, 1, 0},
            Register{MonSensAcbDem, 102, 1, 4, 0},
            Register{MonSensAcbSelBias, 102, 0, 1, 0},
            Register{VrefRsensBot, 103, 8, 1, 0},
            Register{VrefRsensTop, 103, 7, 1, 0},
            Register{VrefIn, 103, 6, 1, 1},
            Register{MonAdcTrim, 103, 0, 6, 0},
            Register{NtcDac, 104, 0, 10, 100},
            Register{HitOrMask3, 105, 0, 6, 0},
            Register{HitOrMask2, 106, 0, 16, 0},
            Register{HitOrMask1, 107, 0, 16, 0},
            Register{HitOrMask0, 108, 0, 16, 0},
            Register{AutoRead0, 109, 0, 9, 137},
            Register{AutoRead1, 110, 0, 9, 133},
            Register{AutoRead2, 111, 0, 9, 121},
            Register{AutoRead3, 112, 0, 9, 122},
            Register{AutoRead4, 113, 0, 9, 124},
            Register{AutoRead5, 114, 0, 9, 127},
            Register{AutoRead6, 115, 0, 9, 126},
            Register{AutoRead7, 116, 0, 9, 125},
            Register{RingOscBClear, 117, 14, 1, 0},
            Register{RingOscBEnBl, 117, 13, 1, 0},
            Register{RingOscBEnBr, 117, 12, 1, 0},
            Register{RingOscBEnCapA, 117, 11, 1, 0},
            Register{RingOscBEnFf, 117, 10, 1, 0},
            Register{RingOscBEnLvt, 117, 9, 1, 0},
            Register{RingOscAClear, 117, 8, 1, 0},
            Register{RingOscAEn, 117, 0, 8, 0},
            Register{RingOscARoute, 118, 6, 3, 0},
            Register{RingOscBRoute, 118, 0, 6, 0},
            Register{RingOscAOut, 119, 0, 16, 0},
            Register{RingOscBOut, 120, 0, 16, 0},
            Register{BcidCnt, 121, 0, 16, 0},
            Register{TrigCnt, 122, 0, 16, 0},
            Register{ReadTrigCnt, 123, 0, 16, 0},
            Register{LockLossCnt, 124, 0, 16, 0},
            Register{BitFlipWngCnt, 125, 0, 16, 0},
            Register{BitFlipErrCnt, 126, 0, 16, 0},
            Register{CmdErrCnt, 127, 0, 16, 0},
            Register{RdWrFifoErrCnt, 128, 0, 16, 0},
            Register{AiRegionRow, 129, 0, 9, 0},
            Register{HitOrCnt3, 130, 0, 16, 0},
            Register{HitOrCnt2, 131, 0, 16, 0},
            Register{HitOrCnt1, 132, 0, 16, 0},
            Register{HitOrCnt0, 133, 0, 16, 0},
            Register{SkippedTrigCnt, 134, 0, 16, 0},
            Register{EfuseReadData1, 135, 0, 16, 0},
            Register{EfuseReadData0, 136, 0, 16, 0},
            Register{MonitoringDataAdc, 137, 0, 12, 0},
    };

    static inline constexpr std::array<std::string_view, 216> names =
            enum_name_list<GlobalRegisterEnum>(std::make_index_sequence<216>{});

    uint16_t read(std::string_view regname) {
        unsigned index = 0;
        for (auto const &n: names) {
            if (regname == n) {
                auto const &reg = regMap[index];
                auto mask = (1 << reg.bits) - 1;
                return ((regs[index] >> reg.offset) & mask);
            }
            index++;
        }
        return 0;
    }

    RegArrayUnpacked regsUnpacked = [] {
          RegArrayUnpacked result{};
          unsigned index = 0;
          for (const auto &r: regMap) {
              result[index++] = r.value;
          }
          return result;
    }();

    inline static RegArray regs = [] {
        RegArray result{};
        for (const auto &r: regMap) {
            auto mask = (1<<r.bits)-1;
            result[r.addr] = (result[r.addr]&(~(mask<<r.offset))) | ((r.value&mask)<<r.offset);
        }
        return result;
    }();

    template<unsigned name>
    constexpr void write(uint64_t value) {
        auto const &reg=regMap[name];
        uint64_t mask = (uint64_t(1)<<reg.bits)-1;
        regs[name] = (regs[name]&(~(mask<<reg.offset))) | ((value&mask)<<reg.offset);
    }
    template<unsigned name>
    static consteval Register const &getReg() {
        return regMap[name];
    }
    template<unsigned name>
    constexpr uint64_t read() {
        auto constexpr &reg=getReg<name>();
        uint64_t mask = 0xffffffffffffffff;
        if constexpr(reg.bits<64) {
            mask = (uint64_t(1) << reg.bits) - 1;
        }
        uint64_t result;
        if constexpr(reg.bits>48) {
            result=uint64_t(regs[name])<<48 |
                    uint64_t(regs[name+1])<<32 |
                    uint64_t(regs[name+2])<<16 |
                    uint64_t(regs[name+3]);
        } else if constexpr (reg.bits>32) {
            result=uint64_t(regs[name])<<32 |
                 uint64_t(regs[name+1])<<16 |
                 uint64_t(regs[name+2]);
        } else if constexpr(reg.bits>16) {
            result=uint64_t(regs[name])<<16 |
                    uint64_t(regs[name+1]);
        } else {
            result = uint64_t(regs[name]);
        }

        return (result >> reg.offset) & mask;
    }
};
