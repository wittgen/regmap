#include "rd53b.h"
#include <string>
#include <nanobind/nanobind.h>

namespace nb = nanobind;
using namespace nb::literals;

template<unsigned N>
void add_property(auto &ref) {
    //GlobalRegister::regMap[N].addr;
    ref.def_prop_rw(std::string(GlobalRegister::names[N]).c_str(), &GlobalRegister::read<N>, &GlobalRegister::write<N>);
    if constexpr(N>0) add_property<N-1>(ref);
}


NB_MODULE(rd53bmod, mod) {
    auto pyClass = nb::class_<GlobalRegister>(mod, "GlobalRegister", nb::dynamic_attr());
    pyClass.def(nb::init());
    add_property<GlobalRegister::N_PROPERTIES-1>(pyClass);
}
