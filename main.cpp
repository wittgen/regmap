#include <iostream>
#include "rd53b.h"
#include "glaze/glaze.hpp"
#include <array>
#include <utility>
#include <tuple>


template<std::size_t Offset, std::size_t ... Is>
static constexpr std::index_sequence<(Offset + Is)...> add_offset(std::index_sequence<Is...>) {
    return {};
}

template<std::size_t Offset, std::size_t N>
static constexpr auto make_index_sequence_with_offset() {
    return add_offset<Offset>(std::make_index_sequence<N>{});
}

template<unsigned N>
auto constexpr static make_item() {
    using T = GlobalRegister;
    return std::make_tuple(T::names[N], glz::custom<&T::write<N>, &T::read<N>>);
}

template<std::size_t ...Is>
static constexpr auto create_tuple(std::index_sequence<Is...>) {
    using T = GlobalRegister;
    return std::tuple_cat(make_item<Is>()...);
}

template<>
struct glz::meta<GlobalRegister> {

    using T = GlobalRegister;
    static constexpr auto t = std::make_tuple("name", glz::custom<&T::write<0>, &T::read<0>>);
    static constexpr auto N = T::N_REGS;
    static constexpr auto t1 = create_tuple(make_index_sequence_with_offset<0, 50>());
    static constexpr auto t2 = create_tuple(make_index_sequence_with_offset<50, 50>());
    static constexpr auto t3 = create_tuple(make_index_sequence_with_offset<100, 50>());
    static constexpr auto t4 = create_tuple(make_index_sequence_with_offset<150, 50>());
    static constexpr auto t5 = create_tuple(make_index_sequence_with_offset<200, 15>());

    static constexpr auto value = std::apply([](auto &&... args) { return object(args...); },
                                             std::tuple_cat(t1, t2, t3, t4, t5));

};

int main() {
    GlobalRegister gr;
    std::string buffer{};
    glz::write_json(gr, buffer);
    std::cout << buffer << "\n";
}
